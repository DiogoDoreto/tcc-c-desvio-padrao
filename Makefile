CC=gcc
CFLAGS=-fopenmp -lm -O3
DEPS = 
OBJ = main.o 

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

dp: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)

clean:
	rm *.o dp
