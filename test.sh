#!/bin/bash

for t in 1 2 4 8; do
  echo threads = $t:
  echo -n > log
  OMP_NUM_THREADS=$t perf stat -d -r 100 ./dp >> log
  awk '$1 == "Duration" {total += $3; lines += 1} END {print "Tempo do algoritmo = " (total / lines) "ms"}' log
  echo ''
done

rm log
